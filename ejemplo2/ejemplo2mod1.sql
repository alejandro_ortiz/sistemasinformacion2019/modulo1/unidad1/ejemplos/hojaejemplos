﻿DROP DATABASE IF EXISTS ejemplo2mod1;
CREATE DATABASE ejemplo2mod1;
USE ejemplo2mod1;

CREATE OR REPLACE TABLE clientes (
  idCliente int AUTO_INCREMENT,
  nombre varchar(100),
  PRIMARY KEY (idCliente)
);

CREATE OR REPLACE TABLE productos (
  idProducto int AUTO_INCREMENT,
  nombre varchar(100),
  peso float,
  idCliente int,
  fecha date,
  cantidad float,
  PRIMARY KEY (idProducto),
  CONSTRAINT fkProductosClientes FOREIGN KEY (idCliente) REFERENCES clientes (idCliente)
);