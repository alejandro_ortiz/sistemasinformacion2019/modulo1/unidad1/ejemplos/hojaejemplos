﻿DROP DATABASE IF EXISTS ejemplo3mod1;
CREATE DATABASE ejemplo3mod1;
USE ejemplo3mod1;

CREATE OR REPLACE TABLE productos (
  idProducto int AUTO_INCREMENT,
  nombre varchar(100),
  peso float,
  PRIMARY KEY (idProducto)
);

CREATE OR REPLACE TABLE clientes (
  idCliente int AUTO_INCREMENT,
  nombre varchar(100),
  PRIMARY KEY (idCliente)
);

CREATE OR REPLACE TABLE compran (
  idProducto int,
  idCliente int,
  cantidad float,
  fecha date,
  PRIMARY KEY (idProducto, idCliente),
  UNIQUE KEY (idProducto),
  UNIQUE KEY (idCliente),
  CONSTRAINT fkCompranProductos FOREIGN KEY (idProducto) REFERENCES productos (idProducto),
  CONSTRAINT fkCompranClientes FOREIGN KEY (idCliente) REFERENCES clientes (idCliente)
);