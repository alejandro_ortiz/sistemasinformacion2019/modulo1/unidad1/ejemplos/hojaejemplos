﻿DROP DATABASE IF EXISTS ejemplo7mod1;
CREATE DATABASE ejemplo7mod1;
USE ejemplo7mod1;

CREATE OR REPLACE TABLE productos (
  idProducto int AUTO_INCREMENT,
  nombre varchar(100),
  peso float,
  PRIMARY KEY (idProducto)
);

CREATE OR REPLACE TABLE clientes (
  idCliente int AUTO_INCREMENT,
  nombre varchar(100),
  PRIMARY KEY (idCliente)
);

CREATE OR REPLACE TABLE telefonos (
  idCliente int,
  telefono varchar(20),
  PRIMARY KEY (idCliente, telefono),
  CONSTRAINT fkTelefonosClientes FOREIGN KEY (idCliente) REFERENCES clientes (idCliente)
);

CREATE OR REPLACE TABLE tienda (
  codigo int AUTO_INCREMENT,
  direccion varchar(100),
  PRIMARY KEY (codigo)
);

CREATE OR REPLACE TABLE compran (
  idProducto int,
  idCliente int,
  codigoTienda int,
  PRIMARY KEY (idProducto, idCliente, codigoTienda),
  UNIQUE KEY (idProducto, codigoTienda),
  CONSTRAINT fkCompranProductos FOREIGN KEY (idProducto) REFERENCES productos (idProducto),
  CONSTRAINT fkCompranClientes FOREIGN KEY (idCliente) REFERENCES clientes (idCliente),
  CONSTRAINT fkCompranTienda FOREIGN KEY (codigoTienda) REFERENCES tienda (codigo)
);

CREATE OR REPLACE TABLE compranDatos (
  idProducto int,
  idCliente int,
  codigoTienda int,
  fecha date,
  cantidad float,
  PRIMARY KEY (idProducto, idCliente, codigoTienda, fecha),
  CONSTRAINT fkDatosCompran FOREIGN KEY (idProducto, idCliente, codigoTienda) REFERENCES compran (idProducto, idCliente, codigoTienda) 
  );