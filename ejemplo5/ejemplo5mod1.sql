﻿DROP DATABASE IF EXISTS ejemplo5mod1;
CREATE DATABASE ejemplo5mod1;
USE ejemplo5mod1;

CREATE OR REPLACE TABLE productos (
  idProducto int AUTO_INCREMENT,
  nombre varchar(100),
  peso float,
  PRIMARY KEY (idProducto)
);

CREATE OR REPLACE TABLE clientes (
  idCliente int AUTO_INCREMENT,
  nombre varchar(100),
  PRIMARY KEY (idCliente)
);

CREATE OR REPLACE TABLE telefonos (
  idCliente int,
  telefono varchar(20),
  PRIMARY KEY (idCliente, telefono),
  CONSTRAINT fkTelefonosClientes FOREIGN KEY (idCliente) REFERENCES clientes (idCliente)
);

CREATE OR REPLACE TABLE compran (
  idProducto int,
  idCliente int,
  PRIMARY KEY (idProducto, idCliente),
  CONSTRAINT fkCompranProductos FOREIGN KEY (idProducto) REFERENCES productos (idProducto),
  CONSTRAINT fkCompranClientes FOREIGN KEY (idCliente) REFERENCES clientes (idCliente)
);

CREATE OR REPLACE TABLE compranDatos (
  idProducto int,
  idCliente int,
  fecha date,
  cantidad float,
  PRIMARY KEY (idProducto, idCliente, fecha),
  CONSTRAINT fkDatosCompran FOREIGN KEY (idProducto, idCliente) REFERENCES compran (idProducto, idCliente)
);