﻿DROP DATABASE IF EXISTS ejemplo3mod1prop;
CREATE DATABASE ejemplo3mod1prop;
USE ejemplo3mod1prop;

CREATE OR REPLACE TABLE productos (
  idProducto int AUTO_INCREMENT,
  nombre varchar(100),
  peso float,
  PRIMARY KEY (idProducto)
);

CREATE OR REPLACE TABLE clientes (
  idCliente int AUTO_INCREMENT,
  nombre varchar(100),
  idProducto int,
  cantidad float,
  fecha date,
  PRIMARY KEY (idCliente),
  UNIQUE KEY (idProducto),
  CONSTRAINT fkClientesProductos FOREIGN KEY (idProducto) REFERENCES productos (idProducto)
);