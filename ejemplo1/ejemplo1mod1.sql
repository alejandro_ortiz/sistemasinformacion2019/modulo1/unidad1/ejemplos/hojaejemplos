﻿DROP DATABASE IF EXISTS ejemplo1mod1;
CREATE DATABASE ejemplo1mod1;
USE ejemplo1mod1;

CREATE OR REPLACE TABLE productos (
  idProducto int AUTO_INCREMENT,
  nombre varchar(100),
  peso float,
  PRIMARY KEY (idProducto)
);

CREATE OR REPLACE TABLE clientes (
  idCliente int AUTO_INCREMENT,
  nombre varchar(100),
  PRIMARY KEY (idCliente)
);

CREATE OR REPLACE TABLE compran (
  idProducto int,
  idCliente int,
  cantidad float,
  fecha date,
  PRIMARY KEY (idProducto, idCliente),
  CONSTRAINT fkCompranProductos FOREIGN KEY (idProducto) REFERENCES productos (idProducto),
  CONSTRAINT fkCompranClientes FOREIGN KEY (idCliente) REFERENCES clientes (idCliente)
);

/** introduccion de datos **/

 INSERT INTO productos (idProducto, nombre, peso) VALUES
(1, 'PRODUCTO 1', 2),
(2, 'PRODUCTO 2', 3),
(3, 'PRODUCTO 3', 4),
(4, 'PRODUCTO 4', 5),
(5, 'PRODUCTO 5', 6),
(6, 'PRODUCTO 6', 7),
(7, 'PRODUCTO 7', 8),
(8, 'PRODUCTO 8', 9),
(9, 'PRODUCTO 9', 10),
(10, 'PRODUCTO 10', 11),
(11, 'PRODUCTO 11', 12),
(12, 'PRODUCTO 12', 13),
(13, 'PRODUCTO 13', 14),
(14, 'PRODUCTO 14', 15),
(15, 'PRODUCTO 15', 16),
(16, 'PRODUCTO 16', 17),
(17, 'PRODUCTO 17', 18),
(18, 'PRODUCTO 18', 19),
(19, 'PRODUCTO 19', 20),
(20, 'PRODUCTO 20', 21);

INSERT INTO clientes (idCliente, nombre) VALUES
(1, 'CLIENTE 1'),
(2, 'CLIENTE 2'),
(3, 'CLIENTE 3'),
(4, 'CLIENTE 4'),
(5, 'CLIENTE 5'),
(6, 'CLIENTE 6'),
(7, 'CLIENTE 7'),
(8, 'CLIENTE 8'),
(9, 'CLIENTE 9'),
(10, 'CLIENTE 10'),
(11, 'CLIENTE 11'),
(12, 'CLIENTE 12'),
(13, 'CLIENTE 13'),
(14, 'CLIENTE 14'),
(15, 'CLIENTE 15'),
(16, 'CLIENTE 16'),
(17, 'CLIENTE 17'),
(18, 'CLIENTE 18'),
(19, 'CLIENTE 19'),
(20, 'CLIENTE 20');

INSERT INTO compran (idProducto, idCliente, fecha, cantidad) VALUES
(1, 18, '2019-06-18 00:00:00', NULL),
(1, 19, '2019-06-19 00:00:00', NULL),
(2, 18, '2019-06-20 00:00:00', NULL),
(3, 1, '2019-06-05 00:00:00', NULL);